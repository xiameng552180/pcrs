#!/usr/bin/env bash

delete_files() {
    local cachedir=${WWWDIR}/${NAME}/static/CACHE

    echo "[DELETE-${NAME}] Deleting files"
    if ! sudo rm -rf ${cachedir}; then
        read -p "[DELETE-${NAME}] Can't sudo: ask an admin to delete '${cachedir}'"
    fi
    rm -rf ${WWWDIR}/${NAME}
    rm -f ${LOGSDIR}/${NAME}.log*
}

delete_db() {
    echo "[DELETE-${NAME}] Deleting database"
    sed -i "/:${NAME}:/d" ${DBLISTFILE}
    dropdb ${NAME}
    if [[ ${SQLINSTANCES} == *${NAME},* ]]; then
        echo "[DELETE-${NAME}] Deleting SQL-instance database"
        sed -i "/:${NAME}_data:/d" ${DBLISTFILE}
        dropdb ${NAME}_data
        if sudo sed -i "/ ${NAME}_data /d" ${POSTGRESCONFFILE}; then
            sudo service postgresql restart
        else
            read -p "[DELETE-${NEWNAME}] Can't sudo: ask an admin to delete '${NAME}_data' from '${POSTGRESCONFFILE}' and restart postgresql"
        fi
    fi
}

# script starts here
if [[ $# -ne 1 ]]; then
	echo "Usage: $0 instance_name"
	exit 1
fi

# vars
THISSCRIPT=$(readlink -f ${BASH_SOURCE})
THISSCRIPTDIR=$(dirname ${THISSCRIPT})
. ${THISSCRIPTDIR}/config.sh
NAME=$1

# main
${THISSCRIPTDIR}/disable_instance.sh ${NAME}
delete_files
delete_db
