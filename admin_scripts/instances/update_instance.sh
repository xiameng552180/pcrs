#!/usr/bin/env bash

update_code() {
    echo "[UPDATE-${NAME}] Updating git code"
    pushd ${DIR} > /dev/null
    git pull --prune
    cd ${PCRSDIR}
    python3 manage.py collectstatic
    popd > /dev/null
}

copy_db() {
    local termdir=${ARCHIVEDIR}/${TERM}

    echo "[UPDATE-${NAME}] Copying database from '${BASENAME}.${TERM}'"
    dropdb --if-exists ${NAME} 2> /dev/null # when invoked by new_instance, this is not needed
    createdb ${NAME}
    cp ${termdir}/${BASENAME}.${TERM}.psql.gz ${DIR}/db.gz
    gzip -d ${DIR}/db.gz
    psql -d ${NAME} -f ${DIR}/db
    rm -f ${DIR}/db
    if [[ ${SQLINSTANCES} == *${BASENAME},* ]]; then
        echo "[UPDATE-${NAME}] Copying SQL-instance database from '${BASENAME}.${TERM}'"
        dropdb --if-exists ${NAME}_data 2> /dev/null # when invoked by new_instance, this is not needed
        createdb ${NAME}_data
        cp ${termdir}/${BASENAME}_data.${TERM}.psql.gz ${DIR}/db_data.gz
        gzip -d ${DIR}/db_data.gz
        psql -d ${NAME}_data -f ${DIR}/db_data
        rm -f ${DIR}/db_data
    fi
}

update_db() {
    local migrationdir=${THISSCRIPTDIR}/migrations

    echo "[UPDATE-${NAME}] Updating database from term '${TERM}'"
    psql -d ${NAME} -f ${migrationdir}/reset.sql
    case ${TERM} in
    "F15")
        psql -d ${NAME} -f ${migrationdir}/F15.sql
        ;&
    "S16")
        psql -d ${NAME} -f ${migrationdir}/S16.sql
        ;&
    "F16")
        psql -d ${NAME} -f ${migrationdir}/F16.sql
        ;&
    "Y17")
        psql -d ${NAME} -f ${migrationdir}/Y17.sql
        ;&
    esac
    pushd ${PCRSDIR} > /dev/null
    python3 manage.py migrate --fake-initial
    popd > /dev/null
}

# script starts here
if [[ $# -lt 2 || $# -gt 3 ]]; then
	echo "Usage: $0 instance_name last_used_term_or_copy_term [copy_instance_name]"
	exit 1
fi

# vars
THISSCRIPT=$(readlink -f ${BASH_SOURCE})
THISSCRIPTDIR=$(dirname ${THISSCRIPT})
. ${THISSCRIPTDIR}/config.sh
NAME=$1
TERM=$2
DIR=${WWWDIR}/${NAME}
PCRSDIR=${DIR}/pcrs

# main
update_code
if [[ $# -eq 3 ]]; then
    BASENAME=$3
    copy_db
fi
update_db
#TODO Replace instructors?
#TODO Drop all student roles for the _data databases?
#TODO Reset logs?
