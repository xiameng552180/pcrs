#!/usr/bin/env bash

new_code() {
    local newcache=${NEWDIR}/static/CACHE
    local csandbox=${NEWPCRSDIR}/languages/c/execution/safeexec_master
    local jsandbox=${NEWPCRSDIR}/languages/java/execution/JavaJail

    echo "[NEW-${NEWNAME}] Downloading git code"
    git clone https://utmandrew@bitbucket.org/utmandrew/pcrs.git ${NEWDIR}
    pushd ${NEWDIR} > /dev/null
    git checkout ${PCRSBRANCH}
    popd > /dev/null
    mkdir -p ${newcache}
    if ! sudo chown www-data:www-data ${newcache}; then
        read -p "[NEW-${NEWNAME}] Can't sudo: ask an admin to chown '${newcache}' to www-data"
    fi
    if [[ ${CINSTANCES} == *${BASENAME},* ]]; then
        echo "[NEW-${NEWNAME}] Creating new C-instance sandbox"
        pushd ${csandbox} > /dev/null
        gcc -o safeexec safeexec.c
        if ! sudo chown root:root safeexec; then
            read -p "[NEW-${NEWNAME}] Can't sudo: ask an admin to chown '${csandbox}/safeexec' to root"
        fi
        if ! sudo chmod 4555 safeexec; then
            read -p "[NEW-${NEWNAME}] Can't sudo: ask an admin to chmod 4555 '${csandbox}/safeexec'"
        fi
        popd > /dev/null
    fi
    if [[ ${JAVAINSTANCES} == *${BASENAME},* ]]; then
        echo "[NEW-${NEWNAME}] Creating new Java-instance sandbox"
        pushd ${NEWPCRSDIR} > /dev/null
        git submodule init
        git submodule update
        cd ${jsandbox}
        ./setup.sh
        popd > /dev/null
    fi
}

new_files() {
    local newlog=${LOGSDIR}/${NEWNAME}.log

    echo "[NEW-${NEWNAME}] Creating new settings copied from '${BASENAME}'"
    cp ${BASESETTINGSDIR}/{settings_local.py,wsgi.py} ${NEWSETTINGSDIR}
    sed -i "s/${BASENAME}/${NEWNAME}/g" ${NEWSETTINGSDIR}/{settings_local.py,wsgi.py}
    if [[ ${SQLINSTANCES} == *${BASENAME},* ]]; then
        sed -i "s/${BASENAME}_data/${NEWNAME}_data/g" ${NEWSETTINGSDIR}/settings_local.py
    fi
    echo "[NEW-${NEWNAME}] Creating new log '${newlog}'"
    touch ${newlog}
}

new_db() {
    local termdir=${ARCHIVEDIR}/${TERM}

    echo "[NEW-${NEWNAME}] Creating new database"
    echo "*:*:${NEWNAME}:pcrsadmin:pcrs" >> ${DBLISTFILE}
    if [[ ${SQLINSTANCES} == *${BASENAME},* ]]; then
        echo "[NEW-${NEWNAME}] Creating new SQL-instance database"
        echo "*:*:${NEWNAME}_data:pcrsadmin:pcrs" >> ${DBLISTFILE}
        SNIPPET="local ${NEWNAME}_data all md5"
        if (echo "${SNIPPET}" | sudo tee -a ${POSTGRESCONFFILE} > /dev/null); then
            sudo service postgresql restart
        else
            echo "[NEW-${NEWNAME}] Can't sudo: ask an admin to add the following to '${POSTGRESCONFFILE}' and restart postgresql"
            read -p "${SNIPPET}"
        fi
    fi
}

suggest_next_steps() {
    echo "[NEW-${NEWNAME}] (Edit ${NEWSETTINGSDIR}/settings_local.py to add or remove supported languages)"
    echo "[NEW-${NEWNAME}] (If you were not an admin for '${BASENAME}.${TERM}', run the following psql command in database '${NEWNAME}')"
    echo "INSERT INTO users_pcrsuser (last_login, username, section_id, is_student, is_ta, is_instructor, is_active, is_admin, is_staff, code_style, use_simpleui) VALUES (NOW(), 'your_username', 'master', FALSE, FALSE, TRUE, TRUE, TRUE, TRUE, 'eclipse', FALSE);"
    if [[ ${SQLINSTANCES} == *${BASENAME},* ]]; then
        echo "[NEW-${NEWNAME}] (If you were not an admin for '${BASENAME}.${TERM}', run the following psql commands in database '${NEWNAME}_data')"
        echo "CREATE USER your_username WITH PASSWORD 'your_username';"
        echo "GRANT instructors TO your_username;"
    fi
}

# script starts here
if [[ $# -ne 3 ]]; then
	echo "Usage: $0 new_instance_name copy_instance_name copy_term"
	exit 1
fi

# vars
THISSCRIPT=$(readlink -f ${BASH_SOURCE})
THISSCRIPTDIR=$(dirname ${THISSCRIPT})
. ${THISSCRIPTDIR}/config.sh
NEWNAME=$1
BASENAME=$2
TERM=$3
NEWDIR=${WWWDIR}/${NEWNAME}
NEWPCRSDIR=${NEWDIR}/pcrs
NEWSETTINGSDIR=${NEWPCRSDIR}/pcrs
BASEDIR=${WWWDIR}/${BASENAME}
BASESETTINGSDIR=${BASEDIR}/pcrs/pcrs

# main
new_code
new_files
new_db
${THISSCRIPTDIR}/update_instance.sh ${NEWNAME} ${TERM} ${BASENAME}
suggest_next_steps
