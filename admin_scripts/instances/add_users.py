#!/usr/bin/env python3

import sys
import csv
import json
from contextlib import ExitStack

# Usage:
#   add_users <name> <current_users_file> <old_users_file> <specs>
# Users files are in csv format
# The specs file is in json format:
# {
#    "user_index": 0, // index for user names in csv user files
#    "default_section_id": "default_id", // the default section id for users if "section_index" and "sections" are not
#                                        // needed, or if a "sections" entry is not found (use null to skip all students
#                                        // without a "sections" entry)
#    "section_index": 2, // [optional] index for user sections in csv user files
#    "sections": { // [optional] the user sections to be found in the csv user files, and the respective section ids
#                  // (use null as section id to skip students in that section);
#                  // uses "default_section_id" as a catch-all
#      "user_section1": "section_id1",
#      "user_section2": "section_id2",
#      "user_section3": null
#    },
#    "is_sql": false // [optional] true if this is a sql instance, false by default
#    "is_mastery": false // [optional] true if this is a mastery instance, false by default
# }


def read_user_file(users_file, user_i, default_section_id, section_i=None, sections=None):

    users = {}
    with open(users_file) as users_csv:
        rows = csv.reader(users_csv, delimiter=",")
        for row in rows:
            user_name = row[user_i]
            if not user_name:
                continue
            user_section_id = default_section_id
            if section_i and sections:
                user_section = row[section_i]
                if user_section in sections:
                    user_section_id = sections[user_section]
            if user_section_id is not None:
                users[user_name] = user_section_id

    return users


if __name__ == '__main__':

    pcrs_name = sys.argv[1]
    new_users_file = sys.argv[2]
    old_users_file = sys.argv[3]
    specs_file = sys.argv[4]
    with open(specs_file) as specs_json:
        specs = json.load(specs_json)
        user_i = specs['user_index']
        default_section_id = specs['default_section_id']
        section_i = specs.get('section_index', None)
        sections = specs.get('sections', None)
        is_sql = specs.get('is_sql', False)
        is_mastery = specs.get('is_mastery', False)

    old_users = read_user_file(old_users_file, user_i, default_section_id, section_i, sections)
    new_users = read_user_file(new_users_file, user_i, default_section_id, section_i, sections)
    with ExitStack() as files:
        sql_file = files.enter_context(open("{}.sql".format(pcrs_name), "w"))
        if is_sql:
            sql_data_file = files.enter_context(open("{}_data.sql".format(pcrs_name), "w"))
        for new_user_name, new_user_section_id in new_users.items():
            if new_user_name in old_users:
                old_user_section_id = old_users.pop(new_user_name)
                if new_user_section_id != old_user_section_id:
                    sql_file.write("UPDATE users_pcrsuser SET section_id = '{}' WHERE username = '{}';\n"
                                   .format(new_user_section_id, new_user_name))
            else:
                mastery_field = ''
                mastery_value = ''
                if is_mastery:
                    mastery_field = ', mastery_time_ratio'
                    mastery_value = ', 1'
                sql_file.write("INSERT INTO users_pcrsuser (last_login, username, section_id, code_style, is_student, "
                               "is_ta, is_instructor, is_active, is_admin, is_staff, use_simpleui{}) "
                               "VALUES (CURRENT_TIMESTAMP, '{}', '{}', 'eclipse', TRUE, FALSE, FALSE, TRUE, FALSE, "
                               "FALSE, FALSE{});\n"
                               .format(mastery_field, new_user_name, new_user_section_id, mastery_value))
                # sometimes, students drop and then add a different section, causing them to remain inactive and to be
                # in the wrong section; the following handles it
                sql_file.write("UPDATE users_pcrsuser SET is_active = TRUE WHERE username = '{}';\n"
                               .format(new_user_name))
                sql_file.write("UPDATE users_pcrsuser SET section_id = '{}' WHERE username = '{}';\n"
                               .format(new_user_section_id, new_user_name))
                if is_sql:
                    sql_data_file.write("CREATE USER {} WITH PASSWORD '{}';\n".format(new_user_name, new_user_name))
                    sql_data_file.write("GRANT students TO {};\n".format(new_user_name))
        # any remaining old_users have dropped and should become inactive
        for old_user_name in old_users:
            sql_file.write("UPDATE users_pcrsuser SET is_active = FALSE WHERE username = '{}';\n".format(old_user_name))
