#!/usr/bin/env bash

archive_files() {
    local termdir=${ARCHIVEDIR}/${TERM}

    echo "[ARCHIVE-${TERM}] Archiving files into '${termdir}'"
    mkdir -p ${termdir}
    crontab -l | gzip > ${termdir}/crontab.${TERM}.gz
    tar -czf ${termdir}/classlists.${TERM}.tgz -C ${USERSDIR} .
    tar -czf ${termdir}/logs.${TERM}.tgz -C ${LOGSDIR} .
}

archive_instances() {
    local termdir=${ARCHIVEDIR}/${TERM}

    echo "[ARCHIVE-${TERM}] Archiving instances into '${termdir}'"
    IFS=',' read -a names <<< ${NAMES}
    for NAME in ${names[@]}; do
        ${THISSCRIPTDIR}/disable_instance.sh ${NAME}
        ${THISSCRIPTDIR}/backup_instance.sh ${NAME} ${TERM}
    done
}

delete_files() {
    echo "[ARCHIVE-${TERM}] Deleting files"
    rm -rf ${USERSDIR}/*
    rm -rf ${LOGSDIR}/*.log.*
    truncate -s 0 ${LOGSDIR}/*.log
}

# script starts here
if [[ $# -ne 2 ]]; then
	echo "Usage: $0 instance_names term"
	exit 1
fi

# vars
THISSCRIPT=$(readlink -f ${BASH_SOURCE})
THISSCRIPTDIR=$(dirname ${THISSCRIPT})
. ${THISSCRIPTDIR}/config.sh
NAMES=$1
TERM=$2

# main
archive_files
archive_instances
delete_files
