import sys
import psycopg2

if len(sys.argv) is not 4:
    print ("USAGE: {} [Database] [Username] [Password]".format(sys.argv[0]))
    exit(1)


getQuests = 'SELECT * FROM content_quest as quests ORDER BY quests.order'
getChallenges = 'SELECT * FROM content_challenge as challenges WHERE challenges.quest_id = {} ORDER BY challenges.order'
updateChallenge = 'UPDATE content_challenge SET x_pos = {}, y_pos = {} WHERE id = {}'

try:
    db = psycopg2.connect(dbname=sys.argv[1], user=sys.argv[2], password=sys.argv[3])
    cursor = db.cursor()

    cursor.execute(getQuests)

    questData = cursor.fetchall()
    for row in questData:
        cursor.execute(getChallenges.format(row[0]))
        x, y = 0, 0
        challengeData = cursor.fetchall()
        for challenge in challengeData:
            print("Setting {} to x={} y={}".format(challenge[1], x, y))
            cursor.execute(updateChallenge.format(x, y, challenge[0]))
            x += 1
            if x == 3:
                x = 0
                y += 1
    db.commit()
    cursor.close()
    exit(0)
except Exception:
    exit(1)
