'use strict'

var gulp = require('gulp')
var less = require('gulp-less')
var concat = require('gulp-concat')
var watch = require('gulp-watch')

/**
 * The directory that we'll serve our static files, for local
 * development.
 * Do not add anything to this directory.
 */
var static_root = 'static/'

/**
 * The directory that we store our local resources.
 * These files will be compiled (optionally), and moved
 * to our static web directory.
 */
var resources = 'resources_updated_ui/'

/**
 * A list of locations to all of our LESS files.
 */
var less_files = [
    // All of our local LESS files
    'pcrs/' + resources + 'less/base.less'
]

/**
 * Watches our local LESS files and recompiles if it
 * notices a change.
 */
gulp.task('watch', function() {
    gulp.watch('pcrs/' + resources + 'less/*.less', ['less'])
});

/**
 * Takes all of our LESS files, compiles them to CSS files,
 * and moves them to the static directory.
 */
gulp.task('less', function() {
    // First, get all of our LESS files
    return gulp.src(less_files)
        .pipe(concat('main.css'))
        // Pass them into our LESS compiler
        .pipe(less())
        // Pipe the compiled file into our static CSS directory
        .pipe(gulp.dest('pcrs/' + resources ))
})
