from django.conf.urls import url

from users.views import UserViewView, UserMasteryQuizView

urlpatterns = [
    url(r'^view_as$', UserViewView.as_view(),
        name='view_as_user'),

    url(r'^mastery_quizzes$', UserMasteryQuizView.as_view(), name='mastery_quizzes')
]
