from django.views.generic import FormView, View
from django.template import loader
from django.http import HttpResponse

from users.forms import SettingsForm, UserForm
from users.section_views import SectionViewMixin
from users.views_mixins import ProtectedViewMixin, CourseStaffViewMixin
from mastery.models import MasteryQuizTestSubmission


class UserSettingsView(ProtectedViewMixin, FormView):
    template_name = 'pcrs/crispy_form.html'
    form_class = SettingsForm

    def form_valid(self, form):
        self.request.user.code_style = form.cleaned_data['colour_scheme']
        self.request.user.save()
        return self.form_invalid(form)

    def get_initial(self):
        initial = super().get_initial()
        initial['colour_scheme'] = self.request.user.code_style
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Settings'
        return context


class UserViewView(CourseStaffViewMixin, FormView):
    """
    A view to select a user to view the pages as.
    """
    template_name = 'pcrs/crispy_form.html'
    form_class = UserForm

    def form_valid(self, form):
        self.request.session['viewing_as'] = form.cleaned_data['user']
        return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'View pages as'
        return context


class UserMasteryQuizView(View, ProtectedViewMixin):
    """
    Displays all of the user's mastery quizzes and scores
    """
    template_name = 'mastery_quizzes.html'

    def get_template_data(self, user_id):
        data = {}
        data['page_title'] = 'Mastery Quizzes'
        data['mastery_quiz_submissions'] = MasteryQuizTestSubmission.get_mastery_quizzes(user_id)
        return data

    def get(self, request, *args, **kwargs):
        template = loader.get_template(self.template_name)
        return HttpResponse(template.render(self.get_template_data(request.user.id), request))


class UserViewMixin(SectionViewMixin):
    """
    A mixin for viewing a page as some other user.
    """

    def get_user(self):
        return self.request.session.get('viewing_as', None) or self.request.user

    def get_section(self):
        user = self.get_user()
        section = user.section if user != self.request.user \
                               else super().get_section()
        return section
