# Celery asynchronous tasks
from __future__ import absolute_import, unicode_literals
from celery import shared_task
import logging
from django.utils.timezone import localtime

logger = logging.getLogger('activity.logging')

@shared_task
def log_activity(request, message):
	logger.info('[{}] [{}] [{}] {}'.format(localtime(), request.META.get('REMOTE_ADDR'), request.user.username, message))