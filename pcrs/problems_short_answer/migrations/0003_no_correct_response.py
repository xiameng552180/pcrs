# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-10-17 20:26
from __future__ import unicode_literals

import django.contrib.postgres.fields.hstore
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('problems_short_answer', '0002_problem_notes'),
    ]

    operations = [
        migrations.AddField(
            model_name='problem',
            name='no_correct_response',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='problem',
            name='keys',
            field=django.contrib.postgres.fields.hstore.HStoreField(blank=True, default=None),
        ),
    ]
