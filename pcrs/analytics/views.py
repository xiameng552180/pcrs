from users.views_mixins import InstructorViewMixin, CourseStaffViewMixin
from django.views.generic import TemplateView
from users.models import PCRSUser, PCRSUserManager
from content.models import Quest
from analytics.analytics_helper import QuestAnalyticsHelper, QuestAnalyticsManager
from mastery.models import MasteryQuizTest, MasteryQuizChallenge
from users.section_views import SectionViewMixin
from django.http import HttpResponse, JsonResponse
import logging

logger = logging.getLogger(__name__)

class AnalyticsView(CourseStaffViewMixin, SectionViewMixin, TemplateView):

	quest_analytics = QuestAnalyticsManager()

	template_name = 'analytics/main.html'

	def get_context_data(self, **kwargs):
		context = super(TemplateView, self).get_context_data(**kwargs)
		context['page_title'] = 'Analytics'
		context['users'] = PCRSUserManager.get_users(True)
		context['quests'] = Quest.objects.all()
		context['mastery_quizzes'] = MasteryQuizChallenge.objects.all()
		analytics = QuestAnalyticsManager()
		return context


class AnalyticsViewAPI(CourseStaffViewMixin, SectionViewMixin):
	''' JSON API helper for our analytics. '''

	quest_analytics = QuestAnalyticsManager()

	def hard_problems(request, number_of_problems):
		response_dict = {}
		response_dict['hard_problems'] = QuestAnalyticsManager().get_hard_problems(number_of_problems, 'easy')
		response_dict['easy_problems'] = QuestAnalyticsManager().get_hard_problems(number_of_problems, 'hard')
		return JsonResponse(response_dict)

	def not_finished(request, mastery_quiz_id):
		''' A list of students who have attempted but not finished (passed) a mastery quiz.'''
		not_finished_students = QuestAnalyticsManager().get_students_not_finished_mastery_quiz(mastery_quiz_id)
		response_dict = {}
		response_dict['mastery_quiz'] = mastery_quiz_id
		response_dict['users_not_finished'] = [str(student) for student in not_finished_students]
		return JsonResponse(response_dict)

	def not_attempted(request, mastery_quiz_id):
		''' A list of students who have not attempted a mastery quiz. '''
		not_attempted_students = QuestAnalyticsManager().get_students_not_attempted_mastery_quiz(mastery_quiz_id)
		response_dict = {}
		response_dict['mastery_quiz'] = mastery_quiz_id
		response_dict['users_not_attempted'] = [str(student) for student in not_attempted_students]
		return JsonResponse(response_dict)




class AnalyticsViewAPI(CourseStaffViewMixin, SectionViewMixin):
	''' JSON API helper for our analytics. '''

	quest_analytics = QuestAnalyticsManager()

	def hard_problems(request, number_of_problems):
		response_dict = {}
		response_dict['hard_problems'] = QuestAnalyticsManager().get_hard_problems(number_of_problems, 'easy')
		response_dict['easy_problems'] = QuestAnalyticsManager().get_hard_problems(number_of_problems, 'hard')
		return JsonResponse(response_dict)

	def not_finished(request, mastery_quiz_id):
		''' A list of students who have attempted but not finished (passed) a mastery quiz.'''
		not_finished_students = QuestAnalyticsManager().get_students_not_finished_mastery_quiz(mastery_quiz_id)
		response_dict = {}
		response_dict['mastery_quiz'] = mastery_quiz_id
		response_dict['users_not_finished'] = [str(student) for student in not_finished_students]
		return JsonResponse(response_dict)

	def not_attempted(request, mastery_quiz_id):
		''' A list of students who have not attempted a mastery quiz. '''
		not_attempted_students = QuestAnalyticsManager().get_students_not_attempted_mastery_quiz(mastery_quiz_id)
		response_dict = {}
		response_dict['mastery_quiz'] = mastery_quiz_id
		response_dict['users_not_attempted'] = [str(student) for student in not_attempted_students]
		return JsonResponse(response_dict)
