from django.conf import settings
from content.models import (Challenge, ContentType, ContentPage, ContentSequenceItem, Quest)
from users.models import PCRSUserManager, PCRSUser
from statistics import median
from mastery.models import MasteryQuizChallenge, MasteryQuizTest, MasteryQuizTestSubmission
from pcrs.models import get_problem_content_types
import problems_java, problems_python, problems_c, problems_sql, problems_ra
import problems_multiple_choice, problems_short_answer
from django.db.models import Avg, Count

problemModels = {
    # These should match the names in INSTALLED_PROBLEM_APPS
    'problems_java': problems_java.models,
    'problems_python': problems_python.models,
    'problems_c': problems_c.models,
    'problems_sql': problems_sql.models,
    'problems_ra': problems_ra.models,
    'problems_multiple_choice': problems_multiple_choice.models,
    'problems_short_answer': problems_short_answer.models,
}

class QuestAnalyticsManager:
    ''' Class that manages computation of the new analytics backend. '''

    def get_students_not_attempted_mastery_quiz(self, mastery_quiz_id):
        ''' Returns a list of student names (utorids) that have not attempted a mastery quiz
        (they have norecorded submission for that mastery quiz).

        Args:
            mastery_quiz_id: The mastery quiz that we want to pull a list of students who have
            not attempted it
        Returns:
            A list of students (utorids) who have not attempted the specified mastery quiz
        '''
        active_students = PCRSUserManager.get_users(True)
        # ID of all of the students who have attempted a problem
        # Get all of the mastery quiz tests associated with this challenge
        mastery_quiz_ids = MasteryQuizTest.objects.filter(mastery_quiz_challenge_id=mastery_quiz_id)
        attempted_students_id = MasteryQuizTestSubmission.objects.filter(mastery_quiz_test_id__in=mastery_quiz_ids).all()
        # Convert them into student objects
        try:
            attempted_students = [PCRSUser.objects.filter(id=student.id).get() for student in attempted_students_id]
        except PCRSUser.DoesNotExist:
            return []
        return [student for student in active_students if student not in attempted_students]

    def get_students_not_finished_mastery_quiz(self, mastery_quiz_id):
        ''' A list of students who have attempted, but not passed a specific mastery quiz.

        Args:
            mastery_quiz_id: The mastery quiz that the students have failed
        Returns:
            A list of students (utorids) who have attempted but not passed the specific mastery quiz
        '''
        # Instead of doing an aggregate query we can simply subtract those that have passed
        query = """
            SELECT users_pcrsuser.id, users_pcrsuser.username
            FROM users_pcrsuser
            JOIN content_MasteryQuizTestSubmission
            ON users_pcrsuser.id = content_MasteryQuizTestSubmission.user_id
            JOIN content_MasteryQuizTest
            ON content_MasteryQuizTest.id = content_MasteryQuizTestSubmission.mastery_quiz_test_id
            JOIN content_MasteryQuizChallenge
            ON content_MasteryQuizChallenge.id = content_MasteryQuizTest.mastery_quiz_challenge_id
            WHERE content_MasteryQuizTestSubmission.score::float / content_MasteryQuizTest.out_of
            >= content_MasteryQuizTest.pass_threshold
            AND content_MasteryQuizChallenge.id = %s
        """
        students_passed = PCRSUser.objects.raw(query, [1])
        # Filter out the students who've passed the mastery quiz
        return [student for student in PCRSUser.objects.all() if not student in students_passed and student not in
            self.get_students_not_attempted_mastery_quiz(mastery_quiz_id)]


    def get_hard_problems(self, number_of_problems, easy_or_hard):
        ''' A list of 'number_of_problems' of the most difficult problems, ordered by the most attempts
        taken before competions.

        Args:
            number_of_problems: The number of hardest problems we want to returns
            easy_or_hard: Returns either the easiest n problems, or hardest n, given by 'easy' or 'hard'
        Returns:
            A list of problem objects
        '''
        # Go through each problem type, and get the median attempts for each problem
        problems = []
        for problem_name, model in problemModels.items():
            # Get all of the problems for this type of problem {java, python ... }
            for problem in model.Problem.objects.all():
                # Get all submissions relating to this problem
                # Make sure there are submissions
                try:
                    problem_attempts = len(model.Submission.objects.filter(problem_id=problem.id).all())
                    if problem_attempts > 0:
                        problems.append({'problem': str(problem), 'attempts': problem_attempts})
                # If a problem doesn't have a submission this will be thrown
                # It's not an error, it just means that nobody has submitted anything for this
                # problem, so we can skip it.
                except model.Submission.DoesNotExist:
                    continue
        # Only return the hardest 'number_of_problems' problems
        if easy_or_hard == 'hard':
            return sorted(problems, key=lambda k: -k['attempts'])[:int(number_of_problems)]
        elif easy_or_hard == 'easy':
            return sorted(problems, key=lambda k: k['attempts'])[:int(number_of_problems)]


    def get_students_many_attempts(self):
        ''' A list of students are struggling (taking a larger number of attempts than average) configurable
        in the settings.

        Returns:
            A list of students who are performing below average on the problems.
        '''
        pass


class QuestAnalyticsHelper:
    '''Used to compute quest analytics'''

    # Unfortunately, we have to iterate each model to get every type of problem

    def __init__(self, quest, users):
        self.users = users
        self.quest = quest

    def getAllIncompleteStudents(self):
        '''Gets a list of students who have not fully completed a given quest'''
        questProblemInfo = self.computeAllProblemInfo()
        incompleteStudents = set()
        for problem in questProblemInfo:
            for student in problem['incompleteUsers']:
                incompleteStudents.add(student)
        return incompleteStudents

    def computeAllProblemInfo(self):
        '''Computes a list of problem information for the given quest

        The quest is specified in the constructor of this object

        Returns:
            A list of problem information.
            See _computeProblemInfoForProblem for format
            This list is sorted in the order of the quest
        '''
        problemInfo = []
        for challenge in Challenge.objects.filter(quest=self.quest):
            for page in ContentPage.objects.filter(challenge=challenge):
                problemInfo += self._getProblemsInfoInPage(page)
        return problemInfo

    def _getProblemsInfoInPage(self, page):
        '''Gathers all problem in the given page.

        To order the problems nicely, this code has to be convoluted >:(
        '''
        problemInfo = []

        for item in ContentSequenceItem.objects.filter(content_page=page):
            contentType = item.content_type
            if contentType.name != "problem":
                # Must be a non-problem. e.g. text or a video
                continue
            if contentType.app_label not in problemModels.keys():
                # If the problem type is not configured
                continue

            problemTypeName = contentType.app_label
            problemModel = problemModels[problemTypeName]

            problemId = item.object_id
            problem = problemModel.Problem.objects.get(pk=problemId)


            problemInfo.append(
                self._computeProblemInfoForProblem(problem, problemTypeName))

        return problemInfo

    def _computeProblemInfoForProblem(self, problem, problemTypeName):
        submissionClass = problemModels[problemTypeName].Submission


        userAttemptCounts = []
        hasSolvedCount = 0
        hasAttemptedCount = 0
        incompleteUsers = set()
        i = 0

        for user in self.users:
            submissionsCount = submissionClass.objects.filter(
                user=user,
                problem=problem
            ).count()
            userAttemptCounts.append(submissionsCount)
            if submissionsCount == 0:
                incompleteUsers.add(user)
                continue

            hasAttemptedCount += 1
            bestSubmission = submissionClass.objects.filter(
                user=user,
                problem=problem,
                has_best_score=True
            )[0]

            if problem.max_score == bestSubmission.score:
                hasSolvedCount += 1
            else:
                incompleteUsers.add(user)

        # Round the median so we don't end up with weird numbers
        medianAttempts = int(median(userAttemptCounts)) \
            if len(userAttemptCounts) else 0

        return {
            'pk': problem.pk,
            'url': problem.get_absolute_url(),
            'name': problem.name,
            'type': settings.INSTALLED_PROBLEM_APPS[problemTypeName],
            'medianAttempts': medianAttempts,
            'hasSolvedCount': hasSolvedCount,
            'hasAttemptedCount': hasAttemptedCount,
            'completedUsers': set(PCRSUserManager.get_users(True)) - set(incompleteUsers),
            'incompleteUsers': incompleteUsers
        }
