from django.conf import settings
from django.conf.urls import url
from analytics.views import AnalyticsView, AnalyticsViewAPI

urlpatterns = [
	# Base Views
    url(r'^analytics$', AnalyticsView.as_view(), name='analytics'),
    # API
    url(r'^api/mastery_quiz/(?P<mastery_quiz_id>[0-9]+)/not_attempted', AnalyticsViewAPI.not_attempted),
    url(r'^api/mastery_quiz/(?P<mastery_quiz_id>[0-9]+)/not_finished', AnalyticsViewAPI.not_finished),
    url(r'^api/problems/hard/(?P<number_of_problems>[0-9]+)', AnalyticsViewAPI.hard_problems)
]