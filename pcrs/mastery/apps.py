from django.apps import AppConfig


class MasteryConfig(AppConfig):
    name = 'mastery'
