from django.db import models
from django.conf import settings
from django.utils.crypto import get_random_string
from django.utils import timezone
from datetime import datetime, timedelta
from django.utils import timezone
from users.models import PCRSUser, PCRSUserManager
from math import ceil
import random
from django.utils import timezone
from content.models import Challenge
from pcrs.settings import MASTERY_QUIZ_EXPIRY_TIME

class MasteryQuizSession(models.Model):
    '''
    A mastery quiz session model responsible for holding the code of the current mastery quiz
    session, as well as what time it starts and ends (so we can destroy the session).
    Fields:
        id
        code
        started_by
        time_started
        time_ended
    '''
    code = models.CharField(max_length=12)
    started_by = models.ForeignKey(PCRSUser)
    time_started = models.DateTimeField(default=timezone.now, blank=True)
    time_ended = models.DateTimeField(null=True)

    @staticmethod
    def get_session_by_code(code, active=True):
        '''
        Returns a mastery quiz session or None, given a code.
        If active is enabled, will only return mastery quiz sessions that have
        not expired.
        '''
        quiz = MasteryQuizSession.objects.filter(code=code).first()
        return None if quiz is None or quiz.join_restricted() else quiz

    @staticmethod
    def generate_code(length=6):
        '''
        Generates a (guaranteed) unique code for our mastery quiz session.
        '''
        code = get_random_string(length=length).lower()
        while MasteryQuizSession.objects.filter(code=code).count() != 0:
            code = get_random_string(length=length).lower()
        return code

    @staticmethod
    def ta_get_mastery_quiz_session(ta_user):
        '''
        Checks if a given TA has a current mastery quiz session running, and returns it.
        Returns None if the TA doesn't have one running.
        '''
        return MasteryQuizSession.objects.filter(started_by=ta_user, time_ended__gt=timezone.now()).first()

    def join_restricted(self):
        '''
        Returns whether or not a user is able to join the quiz.
        '''
        if settings.MASTERY_QUIZ_EXPIRY_TIME == 0:
            return False
        return timezone.now() > self.time_ended

    @staticmethod
    def get_user_active_mastery_quiz(student):
        '''
        Returns whether or not a student is in an active mastery quiz session.
        '''
        # Get all mastery quiz sessions that this student is in
        return MasteryQuizSessionParticipant.objects.filter(
            user=student, 
            time_joined__lte=timezone.now(), 
            end_time__gte=timezone.now()).first()

    @staticmethod
    def get_user_timelimit(student):
        '''
        Gets the timelimit in seconds of how long the user has to complete the quiz.
        '''
        return ceil(settings.MASTERY_QUIZ_DURATION * student.mastery_time_ratio)

    def has_participant(self, student):
        '''
        Returns whether or not the student is a participant of this mastery quiz session. This means that they have
        received the code from the instructor and entered it on the website.
        '''
        return MasteryQuizSessionParticipant.objects.filter(user=student, mastery_quiz_session=self).exists()


class MasteryQuizSessionParticipant(models.Model):
    '''
    A mastery quiz session participant, responsible for linking users to a specific mastery quiz session.
    Fields:
        id
        mastery_quiz_session_id
        time_joined
        user_id
    '''
    mastery_quiz_session = models.ForeignKey(MasteryQuizSession)
    time_joined = models.DateTimeField()
    end_time = models.DateTimeField()
    user = models.ForeignKey(PCRSUser)
    mastery_quiz_challenge = models.ForeignKey('MasteryQuizChallenge')
    mastery_quiz_test = models.ForeignKey('MasteryQuizTest')

    def expired(self):
        '''
        Returns whether or not the mastery quiz is expired (for this student).
        '''
        return timezone.now() > self.end_time

    @staticmethod
    def get_random_mastery_test(mastery_quiz, student):
        '''
        Returns a random Mastery Quiz, prioritizing non-attempted mastery quizzes
        '''
        # Get all of the tests that belong to this mastery challenge
        mastery_tests = [test.id for test in MasteryQuizTest.objects.filter(mastery_quiz_challenge=mastery_quiz).all()]
        attempted_quizzes = [test.mastery_quiz_test_id for test in 
            MasteryQuizSessionParticipant.objects.filter(user=student).all()]
        # They've attempted all of these, return one at random
        not_completed = set(mastery_tests) - set(attempted_quizzes)
        if len(mastery_tests) == 0:
            raise ValueError("No mastery tests found for {}".format(mastery_quiz.name))
        elif len(list(not_completed)) == 0:
            return random.choice(mastery_tests)
        # Else, return one at random from the ones they haven't attempted
        return random.sample(not_completed, 1)[0]

class MasteryQuizChallenge(models.Model):
    '''
    Mastery quiz required for Challenge progression. Each MasteryQuizChallenge can have multiple
    MasteryQuizTests (since we need unique tests).
    Fields:
        id
        description
        out_of
        pass_threshold
        container
    '''
    name = models.CharField(max_length=255)
    quest = models.ForeignKey('content.Quest', null=True)
    requires = models.ForeignKey('MasteryQuizChallenge', null=True)

    @staticmethod
    def has_passed_quiz(user, mastery_quiz_test):
        '''
        Return True if a user has passed a mastery quiz test.
        '''
        quiz_sessions = MasteryQuizSessionParticipant.objects.filter(mastery_quiz_test=mastery_quiz_test).all()
        # Go through each session, get all of the problems, get their submissions, sum the scores
        for session in quiz_sessions:
            challenge = Challenge.objects.get(mastery_quiz=session.mastery_quiz_test)
            total_marks = session.mastery_quiz_test.get_total_marks(user, session)
            return float(total_marks['score']) / float(total_marks['out_of']) > mastery_quiz_test.pass_threshold
        return False

    @staticmethod
    def has_passed_mastery_challenge(user, challenge):
        challenge_tests = MasteryQuizTest.objects.filter(mastery_quiz_challenge=challenge).all()
        for test in challenge_tests:
            if MasteryQuizChallenge.has_passed_quiz(user, test):
                return True
        return False

    @staticmethod
    def get_current_mastery_quiz(student):
        '''
        Returns the mastery quiz challenge that the student is currently on (needs to complete).
        '''
        # Traverse each mastery quiz to see if the user has completed it, and then
        # check if they have the required quest completed.
        current_quiz = MasteryQuizChallenge.objects.filter(requires=None).first()
        # Check to see if they've completed the first one
        while MasteryQuizChallenge.has_passed_mastery_challenge(student, current_quiz):
            # If they don't have the quest requirements for this quiz, return the previous one
            current_quiz = MasteryQuizChallenge.objects.filter(requires=current_quiz).get()
        return current_quiz

    @staticmethod
    def user_passed_mastery_quiz_quest(user, quest):
        # If there is no mastery quiz associated with this quest, pass them by default
        if not MasteryQuizChallenge.objects.filter(quest_id=quest.id).exists():
            return True
        else:
            mastery_quiz_challenge = MasteryQuizChallenge.objects.filter(quest_id=quest.id).first()
            return MasteryQuizChallenge.has_passed_quiz(user, mastery_quiz_challenge.id)

    @staticmethod
    def user_passed_mastery_quiz_quest(user_id, quest):
        # If there is no mastery quiz associated with this quest, pass them by default
        quiz = MasteryQuizChallenge.get_mastery_quiz_for_quest(quest)
        if quiz:
           return True
        return True

    @staticmethod
    def get_mastery_quiz_for_quest(quest):
        return MasteryQuizChallenge.objects.filter(quest_id=quest.id).first()


class MasteryQuizTest(models.Model):
    '''
    Mastery quiz test. There are multiple tests per mastery quiz, so the students get unique tests and don't re-write
    the same one.
    Fields:
        id
        name
        out_of
        mastery_quiz_challenge
    '''
    name = models.CharField(max_length=255, null=True)
    out_of = models.SmallIntegerField()
    pass_threshold = models.FloatField()
    mastery_quiz_challenge = models.ForeignKey('MasteryQuizChallenge')

    def pass_threshold_percentage(self):
        '''
        Returns the pass threshold as a percentage, out of 100.
        '''
        return round(self.pass_threshold * 100)

    def get_total_marks(self, student, session):
        '''
        Returns the user's scaled summed marks, as well as the out of marks.
        '''
        problems = self.get_user_marks_per_problem(student, session)
        score = 0
        out_of = 0
        for problem in problems:
            score += problem['best_score_scaled']
            out_of += problem['out_of_scaled']
        return {'score': score, 'out_of': out_of, 'percentage': round(float(score) / float(out_of) * 100)}

    def get_user_marks_per_problem(self, student, session):
        '''
        Returns a list of best-scoring submissions on this mastery quiz test, given by a user.
        '''
        problems = Challenge.objects.get(mastery_quiz=self).get_problems()
        problems_list_dict = []
        for problem in problems:
            problems_list_dict.append({
                'best_score_scaled': problem.get_best_mastery_score(student, session),
                'out_of_scaled': round(problem.max_score * problem.scaling_factor),
                'name': problem.name
            })
        return problems_list_dict

    def get_out_of_score(self):
        '''
        Returns the out_of score of a mastery quiz test.
        '''
        # Get the challenge belonging to this mastery quiz test
        challenge = Challenge.objects.filter(mastery_quiz=self).first()
        # Challenge doesn't exist
        if challenge is None:
            return 0
        problems = challenge.get_problems()
        score = 0
        for problem in problems:
            score += problem.max_score
        return score


class MasteryQuizTestSubmission(models.Model):
    '''
    Mastery quiz submissions required for Challenge progression
    Fields:
        id
        user_id
        score
        date_marked
        mastery_quiz_id
    '''
    score = models.SmallIntegerField()
    user = models.ForeignKey(PCRSUser)
    date_marked = models.DateTimeField(default=timezone.now, blank=True)
    mastery_quiz_test = models.ForeignKey('MasteryQuizTest')

    @staticmethod
    def get_mastery_quizzes(user_id):
        return MasteryQuizSubmission.objects.filter(user_id=user_id).all()
