from django.views.generic import TemplateView, View
from users.models import PCRSUser, PCRSUserManager
from content.models import Quest, Challenge
from analytics.analytics_helper import QuestAnalyticsHelper, QuestAnalyticsManager
from users.section_views import SectionViewMixin
from django.http import HttpResponse, JsonResponse
from mastery.models import MasteryQuizSession, MasteryQuizSessionParticipant, \
    MasteryQuizTest, MasteryQuizChallenge, MasteryQuizTestSubmission
from django.views.decorators.http import require_http_methods
from django.utils import timezone
from django.shortcuts import render, redirect
from datetime import datetime, timedelta
from django.conf import settings
from math import floor
from django.contrib.auth.decorators import login_required, user_passes_test
from login import is_instructor, is_course_staff, is_ta
from content.models import Challenge


class MasteryCreationView:

    @require_http_methods(['GET'])
    @login_required
    @user_passes_test(is_instructor)
    def edit_mastery_quizzes(request):
        data = {}
        data['page_title'] = 'Edit Mastery Quizzes'
        data['mastery_quizzes'] = []
        data['quests'] = Quest.objects.all()
        data['content_mastery_challenges'] = Challenge.objects.filter(quest_id__isnull=True).all()
        mastery_quizzes = MasteryQuizChallenge.objects.order_by('id').all()
        # Go through and get all of the tests for each challenge
        for quiz_challenge in mastery_quizzes:
            mastery_quiz_tests = MasteryQuizTest.objects.filter(mastery_quiz_challenge=quiz_challenge).order_by(
                'id').all()
            tmp_list = []
            for test in mastery_quiz_tests:
                mastery_challenge = Challenge.objects.filter(mastery_quiz=test).first()
                tmp_list.append({'problem_challenge': mastery_challenge, 'test': test})
            data['mastery_quizzes'].append({'challenge': quiz_challenge, 'tests': tmp_list})
        return render(request, 'mastery/edit_mastery_quizzes.html', data)


class MasteryEditAPI:

    @require_http_methods(['POST'])
    @login_required
    @user_passes_test(is_instructor)
    def save_quiz_challenge(request):
        '''
        Saves the modification of a mastery quiz challenge.
        '''
        quiz_challenge = MasteryQuizChallenge.objects.filter(id=request.POST.get('id')).first()
        if quiz_challenge is None:
            return JsonResponse({'status': 'error', 'message': 'Mastery Quiz Challenge does not exist.'})
        # TODO: REMOVE (TEMPORARY)
        quiz_challenge.out_of = 100
        # END TODO
        quiz_challenge.name = request.POST.get('name')
        quiz_challenge.quest_id = request.POST.get('mastery_quiz_challenge_id')
        quiz_challenge.requires_id = request.POST.get('mastery_quiz_requires_id')
        if quiz_challenge.requires_id == "none":
            quiz_challenge.requires_id = None
        quiz_challenge.save()
        return JsonResponse({'status': 'success'})

    @require_http_methods(['GET'])
    @login_required
    @user_passes_test(is_instructor)
    def get_challenges(request):
        '''
        Returns all of the quiz challenges.
        '''
        challenges = Challenge.objects.filter(quest_id__isnull=True).all().values()
        challenges_list = [x for x in challenges]
        return JsonResponse({'challenges': challenges_list})

    @require_http_methods(['POST'])
    @login_required
    @user_passes_test(is_instructor)
    def add_quiz_challenge(request):
        '''
        Add a quiz challenge, which we can add tests to.
        '''
        MasteryQuizChallenge(name='New Quiz Challenge').save()
        return JsonResponse({'status': 'success'})

    @require_http_methods(['POST'])
    @login_required
    @user_passes_test(is_instructor)
    def delete_quiz_challenge(request):
        '''
        Deletes a quiz challenge.
        WARNING: This will also delete all quiz tests, and quiz test submissions associated with it.
        '''
        quiz_challenge = MasteryQuizChallenge.objects.filter(id=request.POST.get('id')).first()
        if quiz_challenge is None:
            return JsonResponse({'status': 'error', 'message': 'Mastery Quiz Challenge does not exist.'})
        # Get all tests associated with this challenge
        quiz_tests = MasteryQuizTest.objects.filter(mastery_quiz_challenge=quiz_challenge).all()
        # Get all submissions associated with each test, and delete them
        for quiz_test in quiz_tests:
            quiz_test_submissions = MasteryQuizTestSubmission.objects.filter(mastery_quiz_test=quiz_test).all()
            for quiz_test_submission in quiz_test_submissions:
                quiz_test_submission.delete()
            quiz_test.delete()
        quiz_challenge.delete()
        return JsonResponse({'status': 'success'})

    @require_http_methods(['POST'])
    @login_required
    @user_passes_test(is_instructor)
    def save_mastery_test(request):
        '''
        Saves (or creates, if not available) a mastery test for a mastery challenge.
        '''
        # Check if the test exists
        # Check if this is a new test
        if request.POST.get('id') == 'new':
            new_test = MasteryQuizTest(
                # TODO: TEMPORARY, REMOVE
                out_of=100,
                pass_threshold=request.POST.get('pass_threshold'),
                name=request.POST.get('name'),
                mastery_quiz_challenge=MasteryQuizChallenge.objects
                    .filter(id=request.POST.get('mastery_quiz_challenge_id')).first()
            )
            new_test.save()
            challenge = Challenge.objects.filter(id=request.POST.get('mastery_quiz_test_challenge_id')).first()
            challenge.mastery_quiz = new_test
            challenge.save()
            # Next, edit the challenge and put in our mastery quiz ID
            return JsonResponse({'status': 'success', 'test_id': new_test.id})
        else:
            mastery_test = MasteryQuizTest.objects.filter(id=request.POST.get('id')).first()
            mastery_test.pass_threshold = request.POST.get('pass_threshold')
            mastery_test.name = request.POST.get('name')
            mastery_test.save()
            challenge = Challenge.objects.filter(id=request.POST.get('mastery_quiz_test_challenge_id')).first()
            challenge.mastery_quiz = mastery_test
            challenge.save()
        return JsonResponse({
            'status': 'success'
        })

    @require_http_methods(['POST'])
    @login_required
    @user_passes_test(is_instructor)
    def delete_mastery_test(request):
        '''
        Deletes a mastery test (if it exists).
        '''
        mastery_test = MasteryQuizTest.objects.filter(id=request.POST.get('id')).first()
        if mastery_test is None:
            return JsonResponse({'status': 'error', 'message': 'Quiz does not exist.'})
        else:
            mastery_test.delete()
            return JsonResponse({'status': 'success', 'message': 'Deleted Mastery Quiz Test.'})


class MasteryTAView:

    @login_required
    @user_passes_test(is_ta)
    def ta_view(request):
        ta_mastery_quiz_session = MasteryQuizSession.ta_get_mastery_quiz_session(request.user)
        data = {}
        # They don't have a mastery quiz session, take them to the 'start page'
        if ta_mastery_quiz_session is None:
            data['page_title'] = 'Mastery Quiz - Setup'
            return render(request, 'mastery/mastery_ta.html', data)
        # They already have a session running, take them to the session in progress page
        else:
            data['page_title'] = 'Mastery Quiz Session ' + ta_mastery_quiz_session.code
            data['quiz'] = ta_mastery_quiz_session
            return render(request, 'mastery/mastery_ta_in_session.html', data)


class StudentMasteryQuiz:

    @login_required
    def quiz_view(request):
        '''
        Just redirect them to the actual quiz challenge.
        '''
        # Get their session, and see which test they have
        user_session = MasteryQuizSession.get_user_active_mastery_quiz(request.user)
        # They don't have an active session, redirect them to the main page
        if user_session is None:
            return redirect('/')
        # Get the challenge, based on the session
        challenge = Challenge.objects.filter(mastery_quiz_id=user_session.mastery_quiz_test_id).order_by('-id').first()
        return redirect('/content/challenges/{}/1'.format(challenge.id))

    @login_required
    def latest_quiz_result_view(request):
        '''
        View that displays scoring information for the user's latest completed quiz.
        '''
        # Get the student's latest session
        latest_session = MasteryQuizSessionParticipant.objects.filter().order_by('-time_joined').first()
        # They don't have any quizzes completed, just redirect them
        if latest_session is None:
            return redirect('/')
        context = {}
        context['page_title'] = 'Viewing results for ' + \
                                latest_session.mastery_quiz_challenge.name + ' - ' + \
                                latest_session.mastery_quiz_test.name
        context['session'] = latest_session.id
        context['test'] = latest_session.mastery_quiz_test
        context['total_marks'] = latest_session.mastery_quiz_test.get_total_marks(request.user, latest_session)
        context['challenge'] = Challenge.objects.get(mastery_quiz_id=latest_session.mastery_quiz_test.id)
        context['problems'] = latest_session.mastery_quiz_test.get_user_marks_per_problem(request.user, latest_session)
        return render(request, 'mastery/latest_quiz_result.html', context=context)


class StudentMasteryQuizAPI:

    @require_http_methods(['POST'])
    def join_session(request):
        '''
        Student view for joining a mastery quiz session. Each student may only be in one mastery
        quiz session at a time.
        '''
        quiz_session = MasteryQuizSession.get_session_by_code(request.POST.get('code'))
        # Make sure they've entered a valid quiz id.
        if quiz_session is None:
            return JsonResponse({
                'status': 'error',
                'message': 'You have entered an invalid quiz session ID.'
            })
        # Check to see if the student is already in a session
        if MasteryQuizSession.get_user_active_mastery_quiz(request.user):
            return JsonResponse({
                'status': 'error',
                'message': 'You are already in a mastery quiz session.'
            })
        # Make sure they're within the time frame for joining a quiz.
        if settings.MASTERY_QUIZ_EXPIRY_TIME > 0:
            if timezone.now() > (quiz_session.time_started + timedelta(0, settings.MASTERY_QUIZ_EXPIRY_TIME)):
                return JsonResponse({
                    'status': 'error',
                    'message': 'Sorry, {} minutes have passed, you are not allowed to join the quiz.'.format(
                        floor(settings.MASTERY_QUIZ_EXPIRY_TIME / 60)
                    )
                })
        # Check if they've already completed all of the Mastery Quizzes
        mastery_challenges = MasteryQuizChallenge.objects.all()
        num_passed = 0
        for challenge in mastery_challenges:
            if MasteryQuizChallenge.has_passed_mastery_challenge(request.user, challenge):
                num_passed += 1
        if num_passed == len(mastery_challenges):
            return JsonResponse({
                'status': 'error',
                'message': 'You have already passed all of the mastery quizzes. Go study!'
            })
        # Everything looks good, allow them to join
        student_challenge = MasteryQuizChallenge.get_current_mastery_quiz(request.user)
        try:
            MasteryQuizSessionParticipant(
                mastery_quiz_session=quiz_session,
                user=request.user,
                time_joined=timezone.now(),
                end_time=timezone.now() + timedelta(0, (
                        settings.MASTERY_QUIZ_EXPIRY_TIME * request.user.mastery_time_ratio)),
                mastery_quiz_challenge=student_challenge,
                mastery_quiz_test_id=MasteryQuizSessionParticipant.get_random_mastery_test(student_challenge,
                                                                                           request.user)
            ).save()
        except ValueError as e:
            return JsonResponse({
                'status': 'error',
                'message': 'Error! {}'.format(str(e))
            })

        return JsonResponse({
            'status': 'success',
            'message': 'Success! You will be taken to the quiz page now.'
        })


class MasteryQuizAPI:

    @require_http_methods(['POST'])
    @login_required
    @user_passes_test(is_ta)
    def stop_session(request):
        '''
        TA view for stopping an active mastery quiz session.
        '''
        ta_quiz_session = MasteryQuizSession.ta_get_mastery_quiz_session(request.user)
        # This TA doesn't have a session running
        if ta_quiz_session is None:
            return JsonResponse({
                'status': 'error',
                'message': 'You do not have a session running.'
            })
        elif ta_quiz_session.time_ended < timezone.now():
            return JsonResponse({
                'status': 'error',
                'message': 'This session has already expired.'
            })
        else:
            # End the session
            ta_quiz_session.time_ended = timezone.now()
            ta_quiz_session.save()
            return JsonResponse({
                'status': 'success',
                'message': 'The mastery quiz session has been successfully closed.'
            })

    @require_http_methods(['POST'])
    @login_required
    @user_passes_test(is_ta)
    def start_session(request):
        '''
        TA view for starting mastery quiz sessions. Each TA may only have one current
        mastery quiz session.
        '''
        mastery_quizzes = MasteryQuizSession.objects.filter(started_by=request.user.id)
        mastery_quiz_list = [x for x in mastery_quizzes if not x.join_restricted()]
        if len(mastery_quiz_list) > 0:
            return JsonResponse({
                'status': 'error',
                'message': 'You already have a session running.'
            })
        # Start the session
        else:
            # Add the session into our DB
            mastery_quiz_session = MasteryQuizSession(
                code=MasteryQuizSession.generate_code(),
                time_started=timezone.now(),
                # Set the default `time_ended` to the expiry time
                # When a TA hits 'end quiz', this time will be changed
                time_ended=timezone.now() + timedelta(0, settings.MASTERY_QUIZ_EXPIRY_TIME),
                started_by=request.user
            )
            mastery_quiz_session.save()
            return JsonResponse({
                'status': 'success',
                'message': 'Successfully started mastery quiz session.',
                'code': mastery_quiz_session.code
            })


class MasteryQuizTimes:

    @require_http_methods(['GET'])
    @login_required
    @user_passes_test(is_instructor)
    @user_passes_test(is_course_staff)
    def template_view(request):
        '''
        Instructor view for editing user mastery quiz alloted times.
        '''
        # Get the users with custom times
        data = {}
        data['custom_time_users'] = PCRSUser.objects.exclude(mastery_time_ratio=1).all()
        data['users'] = PCRSUserManager().get_students()
        data['page_title'] = 'Edit Mastery Quiz Time Ratios'
        return render(request, 'mastery/mastery_quiz_times.html', data)

    @require_http_methods(['POST'])
    @login_required
    @user_passes_test(is_instructor)
    @user_passes_test(is_course_staff)
    def add_edit_custom_time(request):
        '''
        Given a username, sets the ratio for the amount of time they have for mastery quizzes.
        '''
        # Get the user
        try:
            user = PCRSUser.objects.filter(username=request.POST.get('username')).get()
        except PCRSUser.DoesNotExist:
            return JsonResponse({'status': 'error', 'message': 'User not found.'})
        user.mastery_time_ratio = request.POST.get('ratio')
        user.save()
        return JsonResponse({'status': 'success', 'message': 'Successfully edited user.'})
